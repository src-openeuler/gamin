Name:		gamin
Version:	0.1.10
Release:        39
Summary:	File Alteration Monitor
License:	LGPLv2
URL:		http://www.gnome.org/~veillard/gamin/
Source0:	http://ftp.gnome.org/pub/GNOME/sources/gamin/0.1/gamin-%{version}.tar.bz2
Patch0: 	0001-Poll-files-on-nfs4.patch
Patch1: 	0002-Fix-compilation-of-recent-glib-removing-G_CONST_RETU.patch

BuildRequires:	gcc automake autoconf libtool glib2-devel

%description
The Gamin package contains a File Alteration Monitor which is
useful for notifying applications of changes to the file system.
Gamin is compatible with FAM.
This package is known to build using an LFS 7.4 platform but
has not been tested.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Provides:	%{name}-static = %{version}-%{release}
Obsoletes:	%{name}-static < %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package	help
Summary: 	Doc files for %{name}
BuildArch:	noarch

%description 	help
The %{name}-help package contains doc files for %{name}.


%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -vif
%configure
%make_build

%install
%make_install
mkdir -p %{buildroot}/%{_sysconfdir}/%{name}
touch %{buildroot}/%{_sysconfdir}/%{name}/gaminrc
touch %{buildroot}/%{_sysconfdir}/%{name}/mandatory_gaminrc
rm -f %{buildroot}/%{_libdir}/*.la

%pre

%preun

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING AUTHORS
%config(noreplace) %{_sysconfdir}/%{name}/gaminrc
%config(noreplace) %{_sysconfdir}/gamin/mandatory_gaminrc
%{_libdir}/*.so*
%{_libexecdir}/gam_server

%files devel
%{_includedir}/fam.h
%{_libdir}/pkgconfig/gamin.pc
%{_libdir}/*.a

%files help
%doc ChangeLog NEWS README TODO doc/*.html doc/*.gif doc/*.txt


%changelog
* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.1.10-39
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Fri Oct 30 2020 wuchaochao <wuchaochao4@huawei.com> - 0.1.10-38
- Type:bufix
- CVE:NA
- SUG:NA
- DESC:remove python2

* Tue Jan  7 2020 JeanLeo <liujianliu.liu@huawei.com> - 0.1.10-37
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: update software package

* Wed Oct 10 2019 luhuaxin <luhuaxin@huawei.com> - 0.1.10-36
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: move AUTHORS to license folder

* Mon Sep 30 2019 luhuaxin <luhuaxin@huawei.com> - 0.1.10-35
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: package rebuild

* Tue Sep 27 2019 luhuaxin <luhuaxin@huawei.com> - 0.1.10-34
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: delete useless comments

* Fri Aug 30 2019 luhuaxin <luhuaxin@huawei.com> - 0.1.10-33
- Package init
